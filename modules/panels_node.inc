<?php

/**
 * @file
 * System Theme++ node module integration
 */

function panels_node_system_theme() {
  return array(
    'panels_node_editing' => array(
      'title' => t('Panels node layout and content pages'),
      'path' => array('node/*/panel_layout', 'node/*/panel_content'),
    ),
  );
}
