<?php

/**
 * @file
 * System Theme++ revisioning module integration
 */

function revisioning_system_theme() {
  return array(
    'revisioning_node' => array(
      'title' => t('Revisioning advanced tabs'),
      'path' => array('node/*/revisions/*/edit', 'node/*/revisions/*/publish', 'node/*/revisions/*/delete'),
    ),
  );
}
