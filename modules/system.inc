<?php

/**
 * @file
 * System Theme++ system module integration
 */

function system_system_theme() {
  return array(
    'batch' => array(
      'title' => t('Batch pages'),
      'path' => array('batch', 'batch/*'),
    ),
  );
}
