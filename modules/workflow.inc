<?php

/**
 * @file
 * System Theme++ workflow module integration
 */

function workflow_system_theme() {
  return array(
    'workflow_editing' => array(
      'title' => t('Workflow content tab'),
      'path' => array('node/*/workflow'),
    ),
    'workflow_pages' => array(
      'title' => t('Workflow user pages'),
      'path' => array('workflow', 'workflow/*'),
    ),
  );
}
