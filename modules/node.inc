<?php

/**
 * @file
 * System Theme++ node module integration
 */

function node_system_theme() {
  return array(
    'node_editing' => array(
      'title' => t('Content editing'),
      'path' => array('node/*/edit', 'node/add', 'node/add/*'),
    ),
    'node_revisions' => array(
      'title' => t('Content revision tab'),
      'path' => array('node/*/revisions'),
    ),
  );
}
