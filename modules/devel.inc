<?php

/**
 * @file
 * System Theme++ node module integration
 */

function devel_system_theme() {
  return array(
    'devel_load' => array(
      'title' => t('Devel load content page'),
      'path' => array('node/*/devel/load'),
    ),
    'devel_render' => array(
      'title' => t('Devel render content page'),
      'path' => array('node/*/devel/render'),
    ),
  );
}
